<?php

beans_add_smart_action( 'wp', 'wst_set_up_post_structure' );
function wst_set_up_post_structure() {
	//Remove title only on pages
	if ( is_page() ) {
		beans_remove_action( 'beans_post_title' );

		beans_add_smart_action( 'beans_post_append_markup', 'wst_display_blocks' );
		function wst_display_blocks() {
			$context         = Timber::get_context(); //what is default role of this template ?
			$post            = new TimberPost();
			$context['post'] = $post;
			$templates       = array( 'blocks.twig' );
			Timber::render( $templates, $context ); //make ingredients ($context) available to this template
		}

	}

}
