<?php
//// Remove Beans Default Styling
//remove_theme_support( 'beans-default-styling' );
beans_remove_action( 'beans_enqueue_uikit_components' );
// Enqueue uikit assets

// CSS

beans_add_smart_action( 'wp_enqueue_scripts', 'wst_enqueue_dev_styles' );


function wst_enqueue_dev_styles() {

	wp_enqueue_style( 'child-style', get_stylesheet_directory_uri() . '/assets/css/style.css' );
}

//Javascript

beans_add_smart_action( 'beans_uikit_enqueue_scripts', 'wst_enqueue_uikit_assets', 5 );

function wst_enqueue_uikit_assets() {

	beans_compiler_add_fragment( 'uikit', array(
		CHILD_URL . '/assets/js/animatedtext.js',
		CHILD_URL . '/assets/js/theme.js'
	), 'js' );


	beans_uikit_enqueue_components( array(
		'contrast',
		'cover',
		'animation',
		'modal',
		'overlay',
		'column',
		'switcher',
		'scrollspy'
	) );
	beans_uikit_enqueue_components( array(
		'sticky',
		'slideshow',
		'slider',
		'lightbox',
		'grid',
		'parallax',
		'dotnav',
		'slidenav',
		'accordion'
	),
		'add-ons' );

}

//google fonts
add_action( 'wp_enqueue_scripts', 'wst_add_google_fonts' );
function wst_add_google_fonts() {

	wp_enqueue_style( 'wst-google-fonts', 'https://fonts.googleapis.com/css?family=https://fonts.googleapis.com/css?family=Arvo:400,700|Source+Sans+Pro:400,400i,600,600i', false );
}

