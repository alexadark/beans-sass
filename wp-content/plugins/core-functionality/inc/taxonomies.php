<?php
add_action('init', 'register_work_categories_taxonomy');

function register_work_categories_taxonomy(){

	$labels = array(
		'name'          => _x( ' Work Categories', 'taxonomy general name', CHILD_TEXT_DOMAIN ),
		'singular_name' => _x( ' Work Category', 'taxonomy singular name', CHILD_TEXT_DOMAIN ),
		'menu_name'     => _x( 'Work Categories', 'taxonomy general name', CHILD_TEXT_DOMAIN ),
		'all_items'     => __( 'All Work Categories', CHILD_TEXT_DOMAIN ),
		'add_new_item'  => __( 'Add new Work Category', CHILD_TEXT_DOMAIN ),
		'edit_item'     => __( 'Edit Work Category', CHILD_TEXT_DOMAIN ),
		'add_new_item'  => __( 'Add New Work Category', CHILD_TEXT_DOMAIN ),
		'update_item'   => __( 'Update Work Category', CHILD_TEXT_DOMAIN ),
	);
	$args   = array(
		'labels'            => $labels,
		'show_in_nav_menu'  => true,
		'hierarchical'      => true,
		'show_admin_column' => true,
	);

	register_taxonomy('work-categories','dishes', $args);
}