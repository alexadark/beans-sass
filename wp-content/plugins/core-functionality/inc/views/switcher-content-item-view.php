<?php
$id          = esc_attr( $layout['crb_markupid'] );
$filter      = $id ? $id : '';
$price = esc_html(carbon_get_the_post_meta('crb_price'));
echo beans_open_markup( 'switcher_content_panel'. $filter . '', 'div', array(
	'class'    => 'uk-panel uk-margin-top uk-animation-slide-right ',
) );
	echo beans_open_markup( 'switcher_content_badge'. $filter . '', 'div', array(
		'class'    => 'uk-panel-badge uk-badge',
	) );
		echo $price;
	echo beans_close_markup( 'switcher_content_badge', 'div' );
	echo beans_open_markup( 'switcher_content_title'. $filter . '', 'h3', array(
		'class'    => 'uk-panel-title',
	) ); ?>
			<a href="<?php the_permalink();?>">
				<?php the_title(); ?>
			</a>
<?php
	echo beans_close_markup( 'switcher_content_title', 'h3' );
	echo beans_open_markup( 'switcher_item'. $filter . '', 'div', array(
		'class'    => 'uk-margin tm-menu-item',
	) );
		echo beans_open_markup( 'switcher_item_grid'. $filter . '', 'div', array(
			'class'    => 'uk-grid uk-grid-small',
		) );
			echo beans_open_markup( 'switcher_item_image'. $filter . '', 'div', array(
				'class'    => 'uk-width-medium-1-4',
			) );
				the_post_thumbnail('thumbnail');
			echo beans_close_markup( 'switcher_item_image', 'div' );

			echo beans_open_markup( 'switcher_item_image_excerpt'. $filter . '', 'div', array(
				'class'    => 'uk-width-medium-3-4 tm-switcher-content uk-flex uk-flex-middle',
			) );
				the_excerpt();
			echo beans_close_markup( 'switcher_item_image_excerpt', 'div' );


		echo beans_close_markup( 'switcher_item_grid', 'div' );

	echo beans_close_markup( 'switcher_item', 'div' );
echo beans_close_markup( 'switcher_content_panel', 'div' );
?>
<!--<div class="uk-panel uk-margin-top uk-animation-slide-right  ">-->
<!--	<div class="uk-panel-badge uk-badge">--><?php //echo $price; ?><!--</div>-->
<!--	<h3 class="uk-panel-title">--><?php //the_title(); ?><!--</h3>-->
<!--	<div class="uk-margin tm-menu-item">-->
<!--		<div class="uk-grid uk-grid-small">-->
<!--			<div class="uk-width-medium-1-4">-->
<!--				--><?php //the_post_thumbnail('thumbnail'); ?>
<!--			</div>-->
<!--			<div class="uk-width-medium-3-4 tm-switcher-content uk-flex uk-flex-middle">-->
<!--				<p>--><?php //the_excerpt(); ?><!--</p>-->
<!--			</div>-->
<!--		</div>-->
<!--	</div>-->
<!--</div>-->