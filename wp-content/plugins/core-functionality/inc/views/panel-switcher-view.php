<?php
$post_type = $layout['crb_select_cpt'];
$tax = $layout['crb_select_tax'];
$terms_choice = $layout['crb_terms_choice'];
$id = esc_attr($layout['crb_markupid']);
$filter      = $id ? $id : '';
echo beans_open_markup( 'panel_switcher'.$filter.'', 'div', array(
	'class'    => 'tm-panel-switcher uk-container uk-container-center
uk-margin-large',
) );
	echo beans_open_markup( 'panel_switcher_nav'.$filter.'', 'ul ', array(
		'class'    => 'uk-subnav tm-subnav-panel-switcher uk-subnav-pill  uk-flex uk-flex-center ',
		'data-uk-switcher' => "{connect:'.switcher-content'}"
	) );
			wst_get_switcher_nav($tax);
	echo beans_close_markup( 'panel_switcher_nav', 'ul' );

	echo beans_open_markup( 'panel-switcher-content'.$filter.'', 'ul', array(
		'class'    => 'uk-switcher switcher-content',
	) );
		wst_get_switcher_content($post_type,$tax);
	echo beans_close_markup( 'panel-switcher-content', 'ul' );
echo beans_close_markup( 'panel_switcher', 'div' );
?>
<!--echo beans_close_markup('','div');-->
<!--<div  class="tm-panel-switcher uk-container uk-container-center-->
<!--uk-margin-large">-->
<!--	<ul class="uk-subnav tm-subnav-panel-switcher uk-subnav-pill  uk-flex uk-flex-center "-->
<!--	    data-uk-switcher="{connect:'.switcher-content'}">-->
<!--		--><?php //wst_get_switcher_nav($tax);?>
<!--	</ul>-->
<!--	<ul class="uk-switcher switcher-content" >-->
<!--		--><?php //wst_get_switcher_content($post_type,$tax); ?>
<!--	</ul>-->
<!--</div>-->